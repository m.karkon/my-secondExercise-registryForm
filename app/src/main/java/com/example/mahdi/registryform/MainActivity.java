package com.example.mahdi.registryform;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText name;
    EditText familyName;
    EditText phoneNumber;
    Button btnSubmit;
    LinearLayout linear;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
    }



    public void bindView(){
        name = (EditText) findViewById(R.id.name);
        familyName = (EditText) findViewById(R.id.family_name);
        phoneNumber = (EditText) findViewById(R.id.phone_number);
        btnSubmit = (Button)findViewById(R.id.btn_submit);
        linear = (LinearLayout) findViewById(R.id.linear_layout);
        btnSubmit.setOnClickListener(this);
        linear.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_submit) {
            Toast.makeText(this, "your information submitted", Toast.LENGTH_LONG).show();
        } else if (view.getId() == R.id.linear_layout) {

            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

    }
}
